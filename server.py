import socket
import threading

DEFAULT_SERVER='localhost'
DEFAULT_PORT=9000
DEFAULT_PACKET_SIZE=1024
MAX_CLIENT_COUNT=10
CLIENT_TIMEOUT=60

clients = []
clients_lock = threading.Lock()

class ThreadedServer:
    def __init__(self, host=DEFAULT_SERVER, port=DEFAULT_PORT):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(MAX_CLIENT_COUNT)
        while True:
            client, address = self.sock.accept()
            print("Client with IP: %s:%s connected" % (address[0], str(address[1])))
            client.settimeout(CLIENT_TIMEOUT)
            clients.append(client)
            threading.Thread(target = self.listenToClient,args = (client,address)).start()

    def listenToClient(self, client, address):
        size = DEFAULT_PACKET_SIZE
        while True:
            try:
                data = client.recv(size)
                if data:
                    response = data
                    with clients_lock:
                        for c in clients:
                            c.sendall(response)
                else:
                    raise error('Client disconnected')
            except:
                client.close()
                return False

if __name__ == "__main__":
    ThreadedServer().listen()
