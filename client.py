import socket
import threading
from time import sleep

def receive_msg(s):
    while True:
        data = s.recv(1024)
        if data:
            #print(data.decode())
            pass
        else:
            sleep(1)


if __name__ == '__main__':

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("localhost", 9000,))
    #thread = threading.Thread(target = receive_msg, args = (s,))
    #thread.start()

    while True:
        i = input("Message: ")
        if i:
            s.send(i.encode())
        else:
            break
        print(s.recv(1024).decode())
    s.close()
